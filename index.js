const express = require("express");
const fs = require("fs")
const app = express();
var mysql = require('mysql');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');


const products = ["Apple","Cherry","Pears","Orange"]
app.use(express.json());

var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'nodelogin'
});

app.use(session({
	secret: 'gthuy12',
	resave: true,
	saveUninitialized: true,
	cookie: {httpOnly: false, secure: false}
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json())

app.get('/login', function(request, response) {
	response.sendFile(path.join(__dirname + '/login.html'));
});

app.post('/auth', function(request, response) {
	var username = request.body.username;
    var password = request.body.password;
    var sql = 'SELECT * FROM accounts WHERE username ="' + username + '" AND password ="' + password + '"'
	if (username && password) {
		connection.query(sql, function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				response.redirect('/');
			} else {
				let ind = fs.readFileSync(__dirname + "/login.html")
				const s = 'Incorret password'
				ind = ind.toString().replace("<!-- alert -->", s);
				response.send(ind);
			}			
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});

app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.send('Welcome back, ' + request.session.username + '!');
	} else {
		response.send('Please login to view this page!');
	}
	response.end();
});

//<!-- SEARCH -->

app.get("/search", (req, res) => {
    let ind = fs.readFileSync(__dirname + "/index.html")
    
    const s = "Could not find product " + req.query.q;
    ind = ind.toString().replace("<!-- SEARCH -->", s);
    //res.setHeader("Content-Security-Policy", "script-src http://localhost:8080")
    //res.setHeader("Content-Security-Policy", "script-src 'none'")
    res.send(ind);
})

app.get ("/js", (req, res )=> {
    res.sendFile(__dirname + "/src.js")
});

app.get("/demo", (req, res) => {
    //res.setHeader("Content-Security-Policy", "script-src http://localhost:8080")
    //res.setHeader("Content-Security-Policy", "script-src 'none'")
    res.sendFile(path.join(__dirname+'/main.html'));
})

app.get("/", (req, res) => {
	if (req.session.loggedin) {
		//res.send('Welcome back, ' + req.session.username + '!');
		const username = req.session.username
		let ind = fs.readFileSync(__dirname + "/index.html")
		const s = products.reduce((a, c) => {
			return `${a}<li>${c}</li>`
		}, "")
		ind = ind.toString().replace("<!-- LIST -->", s);
		ind = ind.toString().replace("<!-- LOGIN -->",username)
		//res.setHeader("Content-Security-Policy", "script-src http://localhost:8080")
		//res.setHeader("Content-Security-Policy", "script-src 'none'")
		res.send(ind);
	} else {
		res.sendFile(path.join(__dirname + '/login.html'));
	}
	//res.end();
})

app.get("/products", (req, res) => {
   res.send(products)
})
app.post("/products", (req, res) => {
    products.push(req.body.name);
    res.send({"Add new product success":true})
})

app.listen(8080);

console.log("Listen to 8080")